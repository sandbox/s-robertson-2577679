<?php

/**
 * @file
 * Core hooks and utility functions for commerce_pos_receipts.
 */

/**
 * Implements hook_libraries_info().
 */
function commerce_pos_receipt_libraries_info() {
  $libraries['jquery-print'] = array(
    'name' => 'jQuery Print Plugin',
    'vendor url' => 'https://github.com/DoersGuild/jQuery.print',
    'version arguments' => array(
      'file' => 'jQuery.print.js',
      'pattern' => '/version\s+([0-9a-zA-Z\.-]+)/',
    ),
    'files' => array(
      'js' => array('jQuery.print.js'),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_theme().
 */
function commerce_pos_receipt_theme($existing, $type, $theme, $path) {
  return array(
    'commerce_pos_receipt' => array(
      'template' => 'commerce-pos-receipt',
      'file' => 'commerce_pos_receipt.theme.inc',
      'path' => $path . '/theme',
      'variables' => array(
        'transaction' => null,
      ),
    ),
    'commerce_pos_receipt_order_info' => array(
      'template' => 'commerce-pos-receipt-order-info',
      'file' => 'commerce_pos_receipt.theme.inc',
      'path' => $path . '/theme',
      'variables' => array(
        'order' => NULL,
        'return' => FALSE,
        'transacton' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function commerce_pos_receipt_menu() {
  $items['printer-testing'] = array(
    'title' => 'Printer testing',
    'page callback' => 'printer_testing_page',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['pos/%/print-receipt'] = array(
    'title' => 'Print Transaction Receipt',
    'page callback' => 'commerce_pos_receipt_print',
    'page arguments' => array(1),
    'access callback' => 'commerce_pos_receipt_print_access',
    'access arguments' => array(1),
    'file' => 'includes/commerce_pos_receipt.pages.inc',
  );

  $items['admin/commerce/pos/receipts'] = array(
    'title' => 'Receipts',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_pos_receipt_settings'),
    'access arguments' => array('administer pos receipt receipts'),
    'file' => 'includes/commerce_pos_receipt.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_commerce_pos_transaction_base_info().
 */
function commerce_pos_receipt_commerce_pos_transaction_base_info() {
  return array(
    'commerce_pos_receipt_base' => array(
      'class' => 'CommercePosReceiptBase',
      'types' => array(
        CommercePosService::TRANSACTION_TYPE_SALE,
        CommercePosService::TRANSACTION_TYPE_RETURN,
      ),
    ),
  );
}

/**
 * Implements hook_permission().
 */
function commerce_pos_receipt_permission() {
  return array(
    'administer pos receipt receipts' =>  array(
      'title' => t('Administer POS receipt settings'),
    ),
  );
}

/**
 * Implements hook_form_alter().
 */
function commerce_pos_receipt_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'commerce_pos_sale':
    case 'commerce_pos_return':
      _commerce_pos_receipt_transaction_form_alter($form, $form_state);
      break;

    case 'commerce_pos_sale_payment':
    case 'commerce_pos_return_payment':
      _commerce_pos_receipt_payment_form_alter($form, $form_state);
      break;
  }
}

/**
 * Adds previous receipt printing functionality to the POS transaction forms.
 */
function _commerce_pos_receipt_transaction_form_alter(&$form, &$form_state) {
  // Attach libraries and JS needed to print a receipt.
  $js_settings = array(
    'commercePosReceipt' => array(
      'cssUrl' => url(drupal_get_path('module', 'commerce_pos_receipt') . '/css/commerce_pos_receipt.css', array(
        'absolute' => TRUE,
      )),
    ),
  );

  $form['#attached']['libraries_load'][] = array('jquery-print');
  $form['#attached']['js'][] = drupal_get_path('module', 'commerce_pos_receipt') . '/js/commerce_pos_receipt.js';

  if (isset($_SESSION['commerce_pos_receipt_previous_transaction'])) {
    $form['parked_transactions']['print_previous'] = array(
      '#markup' => l(t('Print Previous Transaction Receipt'), 'pos/' . $_SESSION['commerce_pos_receipt_previous_transaction'] . '/print-receipt', array(
        'attributes' => array(
          'class' => array('use-ajax', 'commerce-pos-receipt-print-previous'),
        ),
      )),
    );
  }

  if (isset($_SESSION['commerce_pos_print_transaction'])) {
    $js_settings['commercePosReceipt']['printInfo'] = array(
      'transactionId' => $_SESSION['commerce_pos_print_transaction'],
      'printUrl' => url('pos/' . $_SESSION['commerce_pos_print_transaction'] . '/print-receipt'),
    );

    unset($_SESSION['commerce_pos_print_transaction']);
  }

  $form['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => $js_settings,
  );
}

/**
 * Adds receipt-printing functionality to the POS payment forms.
 */
function _commerce_pos_receipt_payment_form_alter(&$form, &$form_state) {
  // Attach libraries and JS needed to print a receipt.
  $js_settings = array(
    'commercePosReceipt' => array(
      'cssUrl' => url(drupal_get_path('module', 'commerce_pos_receipt') . '/css/commerce_pos_receipt.css', array(
        'absolute' => TRUE,
      )),
    ),
  );

  $form['#attached']['libraries_load'][] = array('jquery-print');
  $form['#attached']['js'][] = drupal_get_path('module', 'commerce_pos_receipt') . '/js/commerce_pos_receipt.js';
  $form['#attached']['js'][] = array(
    'type' => 'setting',
    'data' => $js_settings,
  );
}

/**
 * Access callback for printing a receipt.
 */
function commerce_pos_receipt_print_access($transaction_id) {
  global $user;

  $access = FALSE;

  if (user_access('administer commerce pos')) {
    $access = TRUE;
  }
  elseif (user_access('process commerce pos sales')) {
    $transaction = CommercePosService::loadTransaction($transaction_id);
    if ($transaction->uid == $user->uid) {
      $access = TRUE;
    }
  }

  return $access;
}

function printer_testing_page() {
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_js(drupal_get_path('module', 'commerce_pos_receipt') . '/js/commerce_pos_receipt.js');
  libraries_load('jquery-print');

  $js_settings = array(
    'commercePosReceipt' => array(
      'cssUrl' => url(drupal_get_path('module', 'commerce_pos_receipt') . '/css/commerce_pos_receipt.css', array(
        'absolute' => TRUE,
      )),
    ),
  );

  drupal_add_js($js_settings, 'setting');

  $output = '';

  $output .= l(t('Click me'), 'pos/1/print-receipt', array('attributes' => array(
    'class' => array('use-ajax'),
  )));

  return $output;
}

/**
 * Builds a render array to be used in a POS receipt body.
 *
 * The array contains various order info, a summary of the line items, order
 * totals, and payments received (and change given if applicable).
 */
function commerce_pos_receipt_build_receipt_body($order) {
  $output = array();

  $rows = array();
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $transaction = CommercePosService::getOrderTransaction($order->order_id);
  $return = $transaction->type == CommercePosService::TRANSACTION_TYPE_RETURN;

  $price_prefix = $return ? '(' : '';
  $price_suffix = $return ? ')' : '';

  // @TODO: make the types of line items that show up here configurable.
  foreach ($wrapper->commerce_line_items as $delta => $line_item) {
    if (!$line_item->value()) {
      // Handle broken line items by not skipping them - not sure what else to do here.
      continue;
    }
    $row = array(
      array(
        'data' => commerce_line_item_title($line_item->value()),
        'class' => array('component-name'),
      ),
      array(
        'data' => $price_prefix . commerce_currency_format(
          $line_item->commerce_total->amount->raw(),
          $line_item->commerce_total->currency_code->raw(),
          $line_item
        ) . $price_suffix,
        'class' => array('component-total'),
      ),
    );

    $first_row = array(
      'data' => $row,
      'class' => array('line-item'),
      'data-line-item-id' => $line_item->line_item_id->raw()
    );

    $second_row = FALSE;

    if ($line_item->__isset('commerce_product') && $line_item->commerce_product->getIdentifier()) {
      $first_row['class'][] = 'has-details';

      $row = array(
        array(
          'data' => t('@sku x@qty @ @price', array(
            '@sku' => $line_item->commerce_product->sku->value(),
            '@qty' => (int) $line_item->quantity->value(),
            '@price' => $price_prefix . commerce_currency_format(
              $line_item->commerce_unit_price->amount->raw(),
              $line_item->commerce_unit_price->currency_code->raw(),
              $line_item
            ) . $price_suffix,
          )),
          'colspan' => 2,
        ),
      );

      $second_row = array(
        'data' => $row,
        'class' => array('line-item-details'),
        'data-line-item-id' => $line_item->line_item_id->raw()
      );
    }

    $rows[] = $first_row;

    if ($second_row) {
      $rows[] = $second_row;
    }
  }

  $rows[count($rows) - 1]['class'][] = 'last';

  $payments = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id));
  $payment_rows = array();

  foreach ($payments as $payment) {
    $row = array(
      array(
        'data' => commerce_payment_method_get_title('title', $payment->payment_method),
        'class' => array('component-label'),
      ),
      array(
        'data' => $price_prefix . commerce_currency_format($payment->amount, $payment->currency_code, $payment) . $price_suffix,
        'class' => array('component-total'),
      ),
    );
    $payment_rows[] = array(
      'data' => $row,
      'class' => array('payment'),
    );
  }

  $output['info'] = array(
    '#theme' => 'commerce_pos_receipt_order_info',
    '#order' => $order,
    '#return' => $return,
    '#transaction' => $transaction,
    '#weight' => -10,
  );

  $output['line_items'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
    '#empty' => 'No Items in order.',
    '#attributes' => array('class' => array('commerce-pos-order')),
    '#weight' => 5,
  );

  $order_total = commerce_pos_price_order_format($wrapper, 'commerce_order_total', $return);
  $output['total']['#markup'] = render($order_total);
  $output['total']['#weight'] = 10;

  $output['payments'] = array(
    '#theme' => 'table',
    '#rows' => $payment_rows,
    '#attributes' => array('class' => array('commerce-pos-order')),
    '#weight' => 15,
  );

  return $output;
}
